export const setActiveBookAction = (bookId: string) => {
  return {
    type: "SELECT_BOOK",
    payload: bookId,
  };
};
