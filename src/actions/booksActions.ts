import { Book } from "../types";

export const addBookAction = (book: Book) => {
  return {
    type: "ADD_BOOK",
    payload: book,
  };
};
