import { Category } from "../types";

export const setActiveCategoryAction = (category: Category) => {
  return {
    type: "SELECT_CATEGORY",
    payload: category,
  };
};
