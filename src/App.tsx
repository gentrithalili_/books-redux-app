import React from "react";
// import TypescriptExample from "./components/typescript/TypescriptExample";
import Categories from "./components/Categories/Categories";
import BookList from "./components/books/BookList";

import "./App.css";
import BookForm from "./components/books/BookForm";

export interface AppProps {}

function App(props: AppProps) {
  return (
    <div className="App mt-2">
      <h1 className="text-center">React Redux App</h1>
      <div className="container">
        <div className="row">
          <div className="col-md-3">
            <Categories />
          </div>
          <div className="col-md-9">
            <BookForm />
            <BookList />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
