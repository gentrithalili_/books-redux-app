import {UserType} from "../components/typescript/User";

export default function formatUser(user: UserType) {
    return `${user.name} - ${user.email} ${user.age ? `- ${user.age}` : ''}`;
}

