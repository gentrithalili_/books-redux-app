import React from "react";
import { Category } from "../../types";
import { connect } from "react-redux";
import { setActiveCategoryAction } from "../../actions/activeCategoryActions";

import "./Categories.scss";

interface Props {
  categories: Category[];
  activeCategory: Category | null;
  dispatch: (action: any) => void;
}

const Categories: React.FC<Props> = (props) => {
  const categories = props.categories as Category[];
  const activeCategory = props.activeCategory;

  const handleOnClick = (category: Category) => {
    props.dispatch(setActiveCategoryAction(category));
  };

  return (
    <div className="Categories">
      <ul className="list-group">
        {categories.map((category) => {
          const isActive = category._id === activeCategory?._id;
          return (
            <li
              key={category._id}
              className={`list-group-item ${isActive ? "active" : ""}`}
              onClick={() => handleOnClick(category)}
            >
              {category.title}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

function mapStateToProps(state: any) {
  return {
    activeCategory: state.activeCategory as Category | null,
    categories: state.categories as Category[],
  };
}

export default connect(mapStateToProps)(Categories);
