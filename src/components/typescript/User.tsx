import React from 'react';
import formatUser from "../../utils/formatUser";

export interface UserType {
    name: string,
    email: string;
    age?: number;
}

interface UserProps {
    user: UserType,
    onClick?: () => void;
}

const User = (props: UserProps) => {
    return (
        <h1>{formatUser(props.user)}</h1>
    );
};

export default User;