import React from 'react';
import User, {UserType} from "./User";
import sum from "../../utils/sum";

const n1 = 100;
const n2 = 20;

const user1: UserType = {name: 'User 1', email: 'user1@gmail.com',}
const user2: UserType = {name: 'User 2', email: 'user2@gmail.com', age: 30}


const TypescriptExample = () => {
    const result = sum(n1, n2);

    return (
        <div>
            <h1>10 + 10 = {result} </h1>
            <User user={user1}  />
            <User user={user2}  onClick={() => console.log('wdwdw')}/>
        </div>
    );
};

export default TypescriptExample;