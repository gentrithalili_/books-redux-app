import React, { useState } from "react";
import { generate as id } from "shortid";
import "./BookForm.scss";
import { useDispatch, useSelector } from "react-redux";
import { addBookAction } from "../../actions/booksActions";
import { Book, Category } from "../../types";

interface Props {}

const initialState = {
  title: "",
  description: "",
  category: "",
};

const BookForm: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [formData, setFormData] = useState(initialState);
  const categories = useSelector(
    (state: any) => state.categories as Category[]
  );

  const disabled =
    !formData.title || !formData.description || !formData.category;

  const handleOnChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleOnSubmit = (event: React.FormEvent) => {
    event.preventDefault();

    if (disabled) {
      return;
    }

    const tempBook: Book = {
      _id: id(),
      title: formData.title,
      desc: formData.description,
      categoryId: formData.category,
    };

    dispatch(addBookAction(tempBook));
    setFormData(initialState);
  };

  return (
    <div className="BookForm">
      <form className="form-inline" onSubmit={handleOnSubmit}>
        <div className="form-group">
          <input
            className="form-control"
            name="title"
            placeholder="Title"
            value={formData.title}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <input
            className="form-control"
            name="description"
            placeholder="Description"
            value={formData.description}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <select
            className="form-control"
            name="category"
            value={formData.category}
            onChange={handleOnChange}
          >
            <option value="" disabled>
              Choose category
            </option>

            {categories.map((category) => (
              <option key={category._id} value={category._id}>
                {category.title}
              </option>
            ))}
          </select>
        </div>

        <div className="form-group">
          <button disabled={disabled} className="btn btn-success">
            Add
          </button>
        </div>
      </form>
    </div>
  );
};

export default BookForm;
