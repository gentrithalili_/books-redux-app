import React from "react";
import { Book as BookType, Category } from "../../types";
import { useDispatch, useSelector } from "react-redux";
import { setActiveBookAction } from "../../actions/activeBookActions";

import "./Book.scss";

interface Props {
  book: BookType;
}

const Book: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const activeBookId = useSelector((state: any) => state.activeBook);
  const categories = useSelector(
    (state: any) => state.categories as Category[]
  );

  const handleOnClick = () => {
    dispatch(setActiveBookAction(props.book._id));
  };

  const category = categories.find(
    (category) => category._id === props.book.categoryId
  );
  const isActive = activeBookId === props.book._id;

  return (
    <li
      className={`Book list-group-item ${isActive ? "active" : ""}`}
      onClick={handleOnClick}
    >
      <h2>{props.book.title}</h2>
      <p>Category: {category?.title}</p>
      {isActive && <p>{props.book.desc}</p>}
    </li>
  );
};

export default Book;
