import React from "react";
import Book from "./Book";
import { Book as BookType, Category } from "../../types";
import { useSelector } from "react-redux";

interface Props {}

const BookList: React.FC<Props> = (props) => {
  const activeCategory = useSelector(
    (state: any) => state.activeCategory as Category | null
  );

  const books = useSelector((state: any) => state.books as BookType[]);

  const filteredBooks = activeCategory
    ? books.filter((book) => book.categoryId === activeCategory._id)
    : books;

  return (
    <div className="BookList">
      <ul className="list-group">
        {!filteredBooks || filteredBooks.length === 0 ? (
          <div className="alert alert-info">No data!</div>
        ) : (
          filteredBooks.map((book) => <Book book={book} key={book._id} />)
        )}
      </ul>
    </div>
  );
};

export default BookList;
