export type Book = {
  _id: string;
  title: string;
  desc: string;
  categoryId: string;
};

export type Category = {
  _id: string;
  title: string;
};
