import { combineReducers } from "redux";
import { booksReducer } from "./booksReducer";
import { categoriesReducer } from "./categoriesReducer";
import { activeCategoryReducer } from "./activeCategoryReducer";
import { activeBookReducer } from "./activeBookReducer";

export const rootReducer = combineReducers({
  books: booksReducer,
  categories: categoriesReducer,
  activeCategory: activeCategoryReducer,
  activeBook: activeBookReducer,
});
