import { AnyAction } from "redux";
import { Category } from "../types";
import { categories } from "../data";

const initialState: Category[] = categories;

export function categoriesReducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    default:
      return state;
  }
}
