import { AnyAction } from "redux";

const initialState: string | null = null;

export function activeBookReducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case "SELECT_BOOK": {
      return state === action.payload ? null : action.payload;
    }
    default:
      return state;
  }
}
