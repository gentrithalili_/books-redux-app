import { AnyAction } from "redux";
import { Book } from "../types";
import { defaultBooks } from "../data";

const initialState: Book[] = defaultBooks;

export function booksReducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case "ADD_BOOK": {
      return [...state, action.payload];
    }
    default:
      return state;
  }
}
