import { AnyAction } from "redux";
import { Category } from "../types";

const initialState: Category | null = null;

export function activeCategoryReducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case "SELECT_CATEGORY": {
      return state?._id === action.payload._id ? null : action.payload;
    }
    default:
      return state;
  }
}
