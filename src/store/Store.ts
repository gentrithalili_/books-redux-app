import { createStore } from "redux";
import { rootReducer } from "../reducers/rootReducer";

export const Store = createStore(rootReducer);

(window as any).Store = Store.getState();
